package com.mk.ssm.web;

import com.mk.ssm.bean.dto.BaseResult;
import com.mk.ssm.bean.entity.Role;
import com.mk.ssm.service.RoleService;
import com.mk.ssm.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yq on 17-4-19.
 * 角色管理
 */
@Api(value = "RoleController",description = "角色控制器",consumes = "application/json")
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;//角色Service注入

    @ApiOperation(value = "添加角色",notes = "传入Role对象进行添加",response = BaseResult.class)
    @ApiImplicitParam(name = "角色",value = "role",required = true,dataType = "BaseResult<Role>",paramType = "Role")
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public BaseResult<Role> createRole(@RequestBody(required = false) Role role){
        BaseResult<Role> result = null;
        if(role == null){
            result = new BaseResult<Role>(false,"需要添加的角色不可为空！");
        }else{
            Integer res = roleService.createRole(role);
            if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM04){
                result = new BaseResult<Role>(true,role);
            }else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM01)
                result = new BaseResult<Role>(false,"权限不足！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM02)
                result = new BaseResult<Role>(false,"传入的值有误！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM05)
                result = new BaseResult<Role>(false,"添加角色时传入的属性缺少，请重新添加！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM06)
                result = new BaseResult<Role>(false,"添加的角色信息中的资源信息有误，请重新添加！");
            else
                result = new BaseResult<Role>(false,"增加角色信息失败！");
        }
        return result;
    }

    @ApiOperation(value = "获取单个角色",notes = "传入id值进行查询",response = BaseResult.class)
    @ApiImplicitParam(name = "角色编号",value = "id",required = true,dataType = "BaseResult<Role>",paramType = "java.lang.Long")
    @RequestMapping(value = "/findById",method = RequestMethod.GET)
    public BaseResult<Role> findRoleById(@RequestParam(value = "id",required = false) Long id){
        BaseResult<Role> result = null;
        if(id == null){
            result = new BaseResult<Role>(false,"查找单个角色时传入的id值不可为空！");
        }else{
            Role role = roleService.findOne(id);
            if(role != null){
                result = new BaseResult<Role>(true,role);
            }else{
                result = new BaseResult<Role>(false,"未查到角色信息！");
            }
        }
        return result;
    }

    @ApiOperation(value = "修改角色",notes = "传入包含id的角色来修改角色信息",response = BaseResult.class)
    @ApiImplicitParam(name = "角色",value = "role",required = true,dataType = "BaseResult<Role>",paramType = "Role")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public BaseResult<Role> updateRole(@RequestBody(required = false) Role role){
        BaseResult<Role> result = null;
        if(role == null){
            result = new BaseResult<Role>(false,"修改时传入的角色不可为空！");
        }else{
            Integer res = roleService.updateRole(role);
            if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM04){
                Role role1 = roleService.findOne(role.getId());
                result = new BaseResult<Role>(true,role1);
            }
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM01)
                result = new BaseResult<Role>(false,"权限不足！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM02)
                result = new BaseResult<Role>(false,"修改时传入的值有误！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM05)
                result = new BaseResult<Role>(false,"需要修改的角色信息不存在！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM06)
                result = new BaseResult<Role>(false,"修改时传入的角色信息中部分资源信息有误，请重新输入修改！");
            else
                result = new BaseResult<Role>(false,"修改角色失败！");
        }
        return result;
    }

    @ApiOperation(value = "删除角色",notes = "传入id来删除角色",response = BaseResult.class)
    @ApiImplicitParam(name = "角色编号",value = "id",required = true,dataType = "BaseResult<Role>",paramType = "java.lang.Long")
    @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
    public BaseResult<Role> deleteRole(@RequestParam(value = "id",required = false) Long id){
        BaseResult<Role> result = null;
        if(id == null){
            result = new BaseResult<Role>(false,"需要删除的角色编号不可为空！");
        }else{
            Integer res = roleService.deleteRole(id);
            if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM04){
                Role role = roleService.findOne(id);
                result = new BaseResult<Role>(true,role);
            }else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM01)
                result = new BaseResult<Role>(false,"需要删除的角色信息下有用户信息，无法删除！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM02)
                result = new BaseResult<Role>(false,"需要删除的角色信息不存在！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM05)
                result = new BaseResult<Role>(false,"超级管理员信息不可删除！");
            else
                result = new BaseResult<Role>(false,"删除角色失败！");
        }
        return result;
    }

    @ApiOperation(value = "查询所有角色",notes = "查询出所有的角色信息",response = BaseResult.class)
    @ApiImplicitParam(name = "不传值",value = "null",required = true,dataType = "BaseResult<List<Role>>",paramType = "Role")
    @RequestMapping(value = "/findAll",method = RequestMethod.GET)
    public BaseResult<List<Role>> findAll(){
        BaseResult<List<Role>> result = null;
        List<Role> list = roleService.findAll();
        if(list != null || list.size() > 0){
            result = new BaseResult<List<Role>>(true,list);
        }else{
            result = new BaseResult<List<Role>>(false,"查询结果为空，目前没有角色信息！");
        }
        return result;
    }
}
