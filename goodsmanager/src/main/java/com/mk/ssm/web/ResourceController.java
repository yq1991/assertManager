package com.mk.ssm.web;

import com.mk.ssm.bean.dto.BaseResult;
import com.mk.ssm.bean.entity.Resource;
import com.mk.ssm.service.ResourceService;
import com.mk.ssm.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yq on 17-4-18.
 * 资源管理
 */
@Api(value = "ResourceController",description = "资源控制器",consumes = "application/json")
@RestController
@RequestMapping("/resource")
public class ResourceController {
    @Autowired
    private ResourceService resourceService;

    /**
     * 添加资源
     * @param resource  需要添加的资源信息
     * @return
     */
    @ApiOperation(value = "添加资源",notes = "传入Resource对象进行添加",response = BaseResult.class)
    @ApiImplicitParam(name = "资源",value = "resource",required = true,dataType = "BaseResult<Resource>",paramType = "Resource")
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public BaseResult<Resource> createResource(@RequestBody(required = false) Resource resource){
        BaseResult<Resource> result = null;
        if(resource == null){
            result = new BaseResult<Resource>(false,"传入的资源不可为空！");
        }else{
            Integer res = resourceService.createResource(resource);
            if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM04)
                result = new BaseResult<Resource>(true,resource);
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM01)
                result = new BaseResult<Resource>(false,"传入的资源父节点未创建，请先创建父节点！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM02)
                result = new BaseResult<Resource>(false,"添加时传入的资源值缺少，请重新添加！");
            else
                result = new BaseResult<Resource>(false,"添加资源信息失败！");
        }
        return result;
    }

    /**
     * 查找单个的资源信息
     * @param id  资源编号
     * @return
     */
    @ApiOperation(value = "获取单个资源",notes = "传入id对象进行获取",response = BaseResult.class)
    @ApiImplicitParam(name = "资源id",value = "id",required = true,dataType = "BaseResult<Resource>",paramType = "java.lang.Long")
    @RequestMapping(value = "/findById",method = RequestMethod.GET)
    public BaseResult<Resource> getResourceById(@RequestParam(value = "id",required = false) Long id){
        BaseResult<Resource> result = null;
        if(id == null){
            result = new BaseResult<Resource>(false,"查询单个资源时传入的id值不可为空！");
        }else{
            Resource resource = resourceService.findOne(id);
            if(resource != null)
                result = new BaseResult<Resource>(true,resource);
            else
                result = new BaseResult<Resource>(false,"该资源信息不存在！");
        }
        return result;
    }

    /**
     * 修改资源信息
     * @param resource  需要修改的资源信息
     * @return
     */
    @ApiOperation(value = "修改资源",notes = "传入需要修改的资源对象",response = BaseResult.class)
    @ApiImplicitParam(name = "资源",value = "resource",required = true,dataType = "BaseResult<Resource>",paramType = "Resource")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public BaseResult<Resource> updateResource(@RequestBody(required = false) Resource resource){
        BaseResult<Resource> result = null;
        if(resource == null){
            result = new BaseResult<Resource>(false,"需要修改的资源不可为空");
        }else{
            Integer res = resourceService.updateResource(resource);
            if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM04) {
                Resource resource1 = resourceService.findOne(resource.getId());
                result = new BaseResult<Resource>(true,resource1);
            }else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM01)
                result = new BaseResult<Resource>(false,"修改资源信息的父节点不存在！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM02)
                result = new BaseResult<Resource>(false,"需要修改的资源不存在！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM05)
                result = new BaseResult<Resource>(false,"需要修改的资源中没有可修改项");
            else
                result = new BaseResult<Resource>(false,"修改资源信息失败！");
        }
        return result;
    }

    /**
     * 删除资源信息
     * @param id  需要删除的资源编号
     * @return
     */
    @ApiOperation(value = "删除资源",notes = "传入需要修改的资源id",response = BaseResult.class)
    @ApiImplicitParam(name = "资源id",value = "id",required = true,dataType = "BaseResult<Resource>",paramType = "java.lang.Long")
    @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
    public BaseResult<Resource> deleteResource(@RequestParam(value = "id",required = false) Long id){
        BaseResult<Resource> result = null;
        if(id == null){
            result = new BaseResult<Resource>(false,"传入的id值不可为空！");
        }else{
            Integer res = resourceService.deleteResource(id);
            if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM04){
                Resource resource = resourceService.findOne(id);
                result = new BaseResult<Resource>(true,resource);
            }else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM01)
                result = new BaseResult<Resource>(false,"需要删除的资源信息不存在！");
            else if(res == ResultUtil.RESULT_CONFIG.RESULT_NUM02)
                result = new BaseResult<Resource>(false,"需要删除的资源信息下还有角色信息，无法删除！");
            else
                result = new BaseResult<Resource>(false,"删除资源信息失败！");
        }
        return result;
    }

    /**
     * 根据条件查询对应的资源信息
     * @param name  资源名称
     * @param url   资源路径
     * @param parentId   资源父节点
     * @param permission 权限
     * @return
     */
    @ApiOperation(value = "查询所有资源",notes = "传入需要查询的信息",response = BaseResult.class)
    @ApiImplicitParam(name = "资源",value = "resource",required = true,dataType = "BaseResult<List<Resource>>",paramType = "Resource")
    @RequestMapping(value = "/findByColumn",method = RequestMethod.GET)
    public BaseResult<List<Resource>> findByColumns(@RequestParam(value = "name",required = false) String name,
                                              @RequestParam(value = "url",required = false)String url,
                                              @RequestParam(value = "parentId" , required = false)Long parentId,
                                              @RequestParam(value = "permission",required = false)String permission){
        BaseResult<List<Resource>> result =null;
        Resource resource = new Resource();
        resource.setName(name);
        resource.setUrl(url);
        resource.setParentId(parentId);
        resource.setPermission(permission);
        List<Resource> list = resourceService.findByColumns(resource);
        if(list != null ||list.size() > 0){
            result = new BaseResult<List<Resource>>(true,list);
        }else{
            result = new BaseResult<List<Resource>>(false,"根据条件查出的集合为空！");
        }
        return result;
    }

}
