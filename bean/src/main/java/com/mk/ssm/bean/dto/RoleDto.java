package com.mk.ssm.bean.dto;

import java.io.Serializable;

/**
 * Created by yq on 17-4-20.
 */
public class RoleDto implements Serializable {
    private Long id; //编号
    private String role; //角色标识 程序中判断使用,如"admin"
    private String description; //角色描述,UI界面显示使用
    private String resourceIds; //拥有的资源
    private Integer able;

    public RoleDto() {
    }

    public RoleDto(String role, String description, String resourceIds, Integer able) {
        this.role = role;
        this.description = description;
        this.resourceIds = resourceIds;
        this.able = able;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }

    public Integer getAble() {
        return able;
    }

    public void setAble(Integer able) {
        this.able = able;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", description='" + description + '\'' +
                ", resourceIds='" + resourceIds + '\'' +
                ", able=" + able +
                '}';
    }
}
