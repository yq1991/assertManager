package com.mk.ssm.bean.entity;

/**
 * Created by yankee on 2017/4/15.
 */
public class OAuthClient {
    private Long id;
    private String clientName;
    private String clientId;
    private String clientSecret;
    private String createAt;
    private String updateAt;
    private Integer able;

    public OAuthClient(Long id, String clientName, String clientId, String clientSecret, String createAt, Integer able) {
        this.id = id;
        this.clientName = clientName;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.createAt = createAt;
        this.able = able;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Integer getAble() {
        return able;
    }

    public void setAble(Integer able) {
        this.able = able;
    }

    @Override
    public String toString() {
        return "OAuthClient{" +
                "id=" + id +
                ", clientName='" + clientName + '\'' +
                ", clientId='" + clientId + '\'' +
                ", clientSecret='" + clientSecret + '\'' +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                ", able=" + able +
                '}';
    }
}
