package com.mk.ssm.service;

import com.mk.ssm.bean.entity.OAuthClient;

import java.util.List;

/**
 * Created by yankee on 2017/4/15.
 */
public interface OAuthClientService {

    OAuthClient createClient(OAuthClient client);// 创建客户端
    OAuthClient updateClient(OAuthClient client);// 更新客户端
    void deleteClient(Long id);// 删除客户端
    OAuthClient findOne(Long id);// 根据id查找客户端
    List<OAuthClient> findAll(Integer able);// 查找所有
    OAuthClient findByClientId(String clientId);// 根据客户端id查找客户端
    OAuthClient findByClientSecret(String clientSecret);//根据客户端安全KEY查找客户端
}
