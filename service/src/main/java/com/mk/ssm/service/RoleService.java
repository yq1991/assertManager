package com.mk.ssm.service;

import com.mk.ssm.bean.entity.Role;

import java.util.List;

/**
 * <p>Resource: yankee
 * <p>Date: 17-4-3
 * <p>Version: 1.0
 * 角色管理
 */
public interface RoleService {


    /**
     * 添加角色
     * @param role
     * @return
     */
    public int createRole(Role role);

    /**
     * 修改角色
     * @param role
     * @return
     */
    public int updateRole(Role role);

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    public int deleteRole(Long roleId);

    /**
     * 根据id查询单个角色信息
     * @param roleId
     * @return
     */
    public Role findOne(Long roleId);

    /**
     * 查询所有角色信息
     * @param
     * @return
     */
    public List<Role> findAll();

    /**
     * 根据角色编号得到角色标识符列表
     * @param roleIds
     * @return
     */
    List<String> findRoles(List<Long> roleIds);

    /**
     * 根据角色编号得到权限字符串列表
     * @param roleIds
     * @return
     */
    List<String> findPermissions(List<Long> roleIds);
}
