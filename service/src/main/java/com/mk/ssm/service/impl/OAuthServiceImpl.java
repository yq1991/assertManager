package com.mk.ssm.service.impl;

import com.mk.ssm.cache.RedisCache;
import com.mk.ssm.service.OAuthClientService;
import com.mk.ssm.service.OAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yankee on 2017/4/15.
 */

@Service
public class OAuthServiceImpl implements OAuthService {

    @Autowired
    private RedisCache cache;

    @Autowired
    private OAuthClientService oAuthClientService;

    @Override
    public void addAuthCode(String authCode, String username) {
        cache.putCache(authCode,username);
    }

    @Override
    public void addAccessToken(String accessToken, String username) {
        cache.putCache(accessToken,username);
    }

    @Override
    public boolean checkAuthCode(String authCode) {
        return cache.getCache(authCode,String.class) != null;
    }

    @Override
    public boolean checkAccessToken(String accessToken) {
        return cache.getCache(accessToken,String.class) != null;
    }

    @Override
    public String getUsernameByAuthCode(String authCode) {
        return cache.getCache(authCode,String.class);
    }

    @Override
    public String getUsernameByAccessToken(String accessToken) {
        return cache.getCache(accessToken,String.class);
    }

    @Override
    public long getExpireIn() {
        return 1L;
    }

    @Override
    public boolean checkClientId(String clientId) {
        return oAuthClientService.findByClientId(clientId) != null;
    }

    @Override
    public boolean checkClientSecret(String clientSecret) {
        return oAuthClientService.findByClientSecret(clientSecret) != null;
    }
}
