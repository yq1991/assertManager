package com.mk.ssm.service.impl;

import com.mk.ssm.bean.entity.OAuthClient;
import com.mk.ssm.mapper.OAuthClientMapper;
import com.mk.ssm.service.OAuthClientService;
import com.mk.ssm.utils.Constants;
import com.mk.ssm.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by yankee on 2017/4/15.
 */

@Service
public class OAuthClientServiceImpl implements OAuthClientService {

    @Autowired
    private OAuthClientMapper oAuthClientDao;

    @Override
    public OAuthClient createClient(OAuthClient client) {
        client.setClientId(UUID.randomUUID().toString());
        client.setClientSecret(UUID.randomUUID().toString());
        client.setCreateAt(TimeUtils.getCurrentTimeInString());
        client.setAble(Constants.ABLE_CONFIG.DEFAULT_ABLE);
        return oAuthClientDao.createClient(client);
    }

    @Override
    public OAuthClient updateClient(OAuthClient client) {
        return oAuthClientDao.updateClient(client);
    }

    @Override
    public void deleteClient(Long id) {
        oAuthClientDao.deleteClient(id);
    }

    @Override
    public OAuthClient findOne(Long id) {
        return oAuthClientDao.findOne(id);
    }

    @Override
    public List<OAuthClient> findAll(Integer able) {
        return oAuthClientDao.findAll(able);
    }

    @Override
    public OAuthClient findByClientId(String clientId) {
        return oAuthClientDao.findByClientId(clientId);
    }

    @Override
    public OAuthClient findByClientSecret(String clientSecret) {
        return oAuthClientDao.findByClientSecret(clientSecret);
    }
}
