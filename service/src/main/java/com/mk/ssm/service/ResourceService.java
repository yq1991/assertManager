package com.mk.ssm.service;

import com.mk.ssm.bean.entity.Resource;

import java.util.List;

/**
 * <p>Resource: yankee
 * <p>Date: 17-4-3
 * <p>Version: 1.0
 */
public interface ResourceService {

    /**
     * 添加资源信息
     * @param resource
     * @return
     */
    public int createResource(Resource resource);

    /**
     * 修改资源信息
     * @param resource
     * @return
     */
    public int updateResource(Resource resource);

    /**
     * 删除资源信息
     * @param resourceId
     * @return
     */
    public int deleteResource(Long resourceId);

    /**
     * 查询单个的资源信息
     * @param resourceId
     * @return
     */
    Resource findOne(Long resourceId);

    /**
     * 查询全部可用的资源信息
     * @return
     */
    List<Resource> findAll();

    /**
     * 根据条件查询出需要的资源信息
     * @param resource
     * @return
     */
    List<Resource> findByColumns(Resource resource);

    /**
     * 得到资源对应的权限字符串
     * @param resourceIds
     * @return
     */
    List<String> findPermissions(List<Long> resourceIds);

    /**
     * 根据用户权限得到菜单
     * @param permissions
     * @return
     */
    List<Resource> findMenus(List<String> permissions);
}
