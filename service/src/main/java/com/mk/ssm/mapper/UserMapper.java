package com.mk.ssm.mapper;

import com.mk.ssm.aop.DataSource;
import com.mk.ssm.bean.dto.UserDto;
import com.mk.ssm.bean.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>Resource: yankee
 * <p>Date: 17-4-3
 * <p>Version: 1.0
 */
public interface UserMapper {
    /**
     * 添加用户
     * @param userDto  用户
     */
    @DataSource("master")
    int createUser(UserDto userDto);

    /**
     * 修改用户
     * @param userDto  用户信息
     */
    @DataSource("master")
    int updateUser(UserDto userDto);

    /**
     * 删除用户
     * @param id  用户编号
     * @param able  用户状态
     */
    @DataSource("master")
    int deleteUser(@Param("id") Long id, @Param("able") Integer able);

    /**
     * 查找单个用户
     * @param id 用户编号
     * @return  单个用户信息
     */
    @DataSource("slave1")
    User findOne(@Param("id") Long id, @Param("able") Integer able);

    /**
     * 查找全部用户
     * @return  用户集合
     */
    @DataSource("slave1")
    List<User> findAll(Integer able);

    /**
     * 根据用户名查询用户信息
     * @param username  用户名
     * @return
     */
    @DataSource("slave2")
    User findByUsername(@Param("username") String username, @Param("able") Integer able);

    /**
     * 根据用户名模糊查询或者id
     * @param id  用户id
     * @param username  登录名
     * @param able  状态
     * @return
     */
    @DataSource("slave2")
    List<User> findUsers(@Param("id") Long id, @Param("username") String username, @Param("able") Integer able);

    List<User> findUsersByOrg(@Param("orgId") Long orgId, @Param("name") String name, @Param("able") Integer able
                                   /* ,@Param("start") Integer start,@Param("rows") Integer rows*/);

    int countNum(@Param("orgId") Long orgId, @Param("name") String name, @Param("able") Integer able);
}
