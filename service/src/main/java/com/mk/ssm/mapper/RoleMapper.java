package com.mk.ssm.mapper;

import com.mk.ssm.bean.dto.RoleDto;
import com.mk.ssm.bean.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>Resource: yankee
 * <p>Date: 17-4-3
 * <p>Version: 1.0
 * 角色管理
 */
public interface RoleMapper {

    /**
     * 添加角色
     * @param roleDto
     * @return
     */
    int createRole(RoleDto roleDto);

    /**
     * 修改角色
     * @param roleDto
     * @return
     */
    int updateRole(RoleDto roleDto);

    /**
     * 删除角色
     * @param roleId
     * @param able
     */
    int deleteRole(@Param("id") Long roleId, @Param("able") Integer able);

    /**
     * 获取单个角色
     * @param id
     * @return
     */
    Role findOne(@Param("id") Long id, @Param("able") Integer able);

    /**
     * 查询全部角色信息
     * @return
     */
    List<Role> findAll(Integer able);
}
