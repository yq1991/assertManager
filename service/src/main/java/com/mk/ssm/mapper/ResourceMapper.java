package com.mk.ssm.mapper;

import com.mk.ssm.bean.entity.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>Resource: yankee
 * <p>Date: 17-4-3
 * <p>Version: 1.0
 */
public interface ResourceMapper {

    /**
     * 添加资源信息
     * @param resource
     */
    int createResource(Resource resource);

    /**
     * 修改资源信息
     * @param resource
     */
    int updateResource(Resource resource);

    /**
     * 删除资源信息
     * @param id
     * @param able
     */
    int deleteResource(@Param("id") Long id, @Param("able") Integer able);

    /**
     * 查询单个的资源信息
     * @param resourceId
     * @return
     */
    Resource findOne(@Param("id") Long resourceId, @Param("able") Integer able);

    /**
     * 产训全部的资源信息
     * @return
     */
    List<Resource> findAll(Integer able);

    /**
     * 根据字段名称来查询资源信息
     * @param resource
     * @return
     */
    List<Resource> findByColumns(Resource resource);
}
