package com.mk.ssm.mapper;

import com.mk.ssm.bean.entity.OAuthClient;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yankee on 2017/4/15.
 */
@Repository
public interface OAuthClientMapper {

    OAuthClient createClient(OAuthClient client);// 创建客户端
    OAuthClient updateClient(OAuthClient client);// 更新客户端
    void deleteClient(@Param("id") Long id);// 删除客户端
    OAuthClient findOne(@Param("id") Long id);// 根据id查找客户端
    List<OAuthClient> findAll(@Param("able") Integer able);// 查找所有
    OAuthClient findByClientId(@Param("clientId") String clientId);// 根据客户端id查找客户端
    OAuthClient findByClientSecret(@Param("clientSecret") String clientSecret);//根据客户端安全KEY查找客户端
}
