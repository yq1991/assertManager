package com.mk.ssm.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wang18365266176 on 17-5-24.
 */
public class DayUtil {
    public static String getDays(String date1, String date2) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1=sdf.parse(date1);
        Date d2=sdf.parse(date2);
        long daysBetween=(d2.getTime()-d1.getTime()+1000000)/(3600*24*1000);
//        System.out.println("1987-01-01 与 2010-01-01 相隔 "+daysBetween+" 天");
        return daysBetween + "";
    }
    public static String getDate(String day, int num) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 日期格式
        Date date = null; // 指定日期
        try {
            date = dateFormat.parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date newDate = null; // 指定日期加上num天
        try {
            newDate = addDate(date, num);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat.format(newDate);
    }
    public static Date addDate(Date date, long day) throws ParseException {
        long time = date.getTime(); // 得到指定日期的毫秒数
        day = day*24*60*60*1000; // 要加上的天数转换成毫秒数
        time+=day; // 相加得到新的毫秒数
        return new Date(time); // 将毫秒数转换成日期
    }
}
