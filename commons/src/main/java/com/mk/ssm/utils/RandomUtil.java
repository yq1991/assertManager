package com.mk.ssm.utils;

import java.util.*;

/**
 * Created by wang18365266176 on 17-5-1.
 */
public class RandomUtil {
    /**
     * 在0～len 之间产生size个随机整数,按照从小到大排序
     * @param size
     * @param len
     * @return
     */
    public static Integer [] generateRandomArray(int size, int len){

        Set<Integer> set = new LinkedHashSet<Integer>(); //集合是没有重复的值,LinkedHashSet是有顺序不重复集合,HashSet则为无顺序不重复集合
        Integer num = size;
        Integer range = len;
        Random ran = new Random();
        while(set.size() < num){
            Integer tmp = ran.nextInt(range); //0-51之间随机选一个数
            set.add(tmp);//直接加入，当有重复值时，不会往里加入，直到set的长度为52才结束
        }
        Integer [] arr = new Integer[set.size()];
        set.toArray(arr);
        quickSort(arr,0,arr.length-1);
        return arr;
    }
    /**
     * 快速排序
     * @param list
     * @param low
     * @param high
     * @return
     */
    public static int getMiddle(Integer[] list, int low, int high) {
        int tmp = list[low];    //数组的第一个作为中轴
        while (low < high) {
            while (low < high && list[high] >= tmp) {
                high--;
            }
            list[low] = list[high];   //比中轴小的记录移到低端
            while (low < high && list[low] <= tmp) {
                low++;
            }
            list[high] = list[low];   //比中轴大的记录移到高端
        }
        list[low] = tmp;              //中轴记录到尾
        return low;                   //返回中轴的位置
    }
    public static void quickSort(Integer [] list, int low, int high) {
        if (low < high) {
            int middle = getMiddle(list, low, high);  //将list数组进行一分为二
            quickSort(list, low, middle - 1);        //对低字表进行递归排序
            quickSort(list, middle + 1, high);       //对高字表进行递归排序
        }
    }

    /**
     * 有代表性的获取数据
     * @param datas
     * @param <T>
     * @return
     */
    public static <T> List<T> hashAllDots(List<T> datas,int sum){
        List<T> hiss = new ArrayList<>();
        Integer [] indexArr = RandomUtil.generateRandomArray(sum,datas.size());
        int len = indexArr.length;
        for (int i=0;i<len;i++){
            hiss.add(datas.get(indexArr[i]));
        }
        return hiss;
    }

    public static int getRandomInt(int max,int min){
        Random random = new Random();
        int res = random.nextInt(max) % (max-min+1) + min;
        return getRandomIntRange10() % 2 == 0? res:0-res;
    }

    public static int getRandomIntRange10(){
        Random random = new Random();
        int res = random.nextInt(10);
        return res;
    }
}
