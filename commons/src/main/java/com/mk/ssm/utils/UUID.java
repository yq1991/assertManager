package com.mk.ssm.utils;

/**
 * Created by yanzhiping on 2017/5/18.
 */
public class UUID {

    /**
     * 生成UUID
     * @return
     */
    public static String RANDOM_UUID(){
        java.util.UUID uuid=java.util.UUID.randomUUID();
        String str = uuid.toString();
        String uuidStr=str.replace("-", "");
        return uuidStr;
    }
}
