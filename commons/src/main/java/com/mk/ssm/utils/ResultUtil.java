package com.mk.ssm.utils;

/**
 * Created by yanzhiping on 2017/4/29.
 */
public class ResultUtil {
    private static final Boolean SUCCESS = true;
    private static final Boolean ERROR = false;

    public static Boolean aopResult(Integer result){
        return result > 0 ? SUCCESS:ERROR;
    }

    public interface RESULT_CONFIG{
        public static final Integer RESULT_NUM01 = -2;
        public static final Integer RESULT_NUM02 = -1;
        public static final Integer RESULT_NUM03 = 0;
        public static final Integer RESULT_NUM04 = 1;
        public static final Integer RESULT_NUM05 = 2;
        public static final Integer RESULT_NUM06 = 3;
        public static final Integer RESULT_NUM07 = -3;
        public static final Integer RESULT_NUM08 = 4;
        public static final Integer RESULT_NUM09 = 5;
        public static final Integer RESULT_NUM10 = 6;
    }
    /**
     * 判断字符串是否为中文
     * 如果是英文或数字则返回true,是中文就返回false
     */
    public static Boolean isChinese(String str){
        for(char c : str.toCharArray()) {
            if ((c + "").getBytes().length != 1) {
                return false;//中文返回false
            }
        }
        return true;//英文或者数字返回true
    }

    public static Boolean haveName(String name){
        char[] str = name.toCharArray();
        for (int i = 0; i < str.length; i++) {
            if('%' == (str[i]))
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(haveName("%"));
        System.out.println(haveName("%aa"));
        System.out.println(haveName("aa%"));
        System.out.println(haveName("a1%"));
        System.out.println(haveName("无1%"));
        System.out.println(haveName("无&"));
    }

    /**
     * 判断输入的用户名是否由字母/数字/下划线组合而成
     */
    public static Boolean matchUserName(String userName){
        return userName != null && userName.matches("[A-Za-z0-9_]+");
    }
}
