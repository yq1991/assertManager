package com.mk.ssm.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页实体类
 * Created by wang18365266176 on 17-4-21.
 */
public class PageBean<T> {
    private Long total;//总数据个数
    private T rows;//每页显示的数据

    public PageBean() {
    }

    public PageBean(Long total, T rows) {
        this.total = total;
        this.rows = rows;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public T getRows() {
        return rows;
    }

    public void setRows(T rows) {
        this.rows = rows;
    }

    /**
     * 分页查询
     * @param list2
     * @param nowPage
     * @param pageSize
     * @param <T>
     * @return
     */
    public static <T> List<T> getInfoByPage(List<T> list2, int nowPage, int pageSize){
        List<T> list=new ArrayList<>();
        int objNum=list2.size();
        int pageSum;
        if (nowPage<=0){
            nowPage=1;
        }
        if (pageSize<=0){
            pageSize=1;
        }
        if (objNum<=pageSize){
            pageSum=1;
        }else {
            if (objNum%pageSize==0){
                pageSum=objNum/pageSize;
            }else {
                pageSum=objNum/pageSize+1;
            }
        }

        if (nowPage>=pageSum)
        {
            nowPage = pageSum;
            for (int i = (nowPage - 1) * pageSize; i < objNum; i++) {
                T t = list2.get(i);
                list.add(t);
            }
            return list;
        }else {
            if (nowPage <pageSum) {
                for (int i = (nowPage - 1) * pageSize; i < nowPage * pageSize; i++) {
                    T t = list2.get(i);
                    list.add(t);
                }
                return list;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "total=" + total +
                ", rows=" + rows +
                '}';
    }
}
