package com.mk.ssm.utils;

/**
 * @author yq
 *
 */
public class Constants {

    public static final String RESOURCE_SERVER_NAME = "jeews-server";
    public static final String INVALID_CLIENT_DESCRIPTION = "客户端验证失败，如错误的client_id/client_secret。";
    public static final String INVALID_OAUTH_CODE = "授权码错误";
    public static final String INVALID_REFRESH_TOKEN = "refresh_token 错误";
    public static final String INVALID_USER_PASSWORD = "用户名或密码错误";

    /**
     * ABLE
     */
    public interface ABLE_CONFIG{
        public static final Integer ABLE = 1;
        public static final Integer UNABLE = 0;
        public static final Integer DEFAULT_ABLE = 1;
    }

    public interface LOCKED_CONFIG{
        public static final Integer LOCKED = 1;
        public static final Integer UNLOCKED = 0;
        public static final Integer DEFAULT_LOCKED = 1;
    }
    public interface PAGE_CONFIG{
        public static final Integer PageNum=1;
        public static final Integer Pagesize=20;
    }

    public interface JWT_CONFIG{
        public static final Integer EXPIRYSECONDS = 60*30;
        public static final String JWTSIGNER = "Mj2/TPTG7qRqlN7vsmDyOA==";
    }

    //最顶层父接口编号
    public interface PARENT_CONFIG{
        public static final Long TOTAL_PARENTID = 0L;
    }

    //报警信息类型
    public interface WARNING_TYPE{
        public static final Long HEART_HEIGHT_WARNING = 1L;
        public static final Long HEART_LOW_WARNING = 2L;
        public static final Long LONG_PRESS_WARNING = 3L;
        public static final Long OVER_WORK_WARNING = 4L;
        public static final Long LOCATION_ABNORMAL_WARNING = 5L;
        public static final Long AREADENSITY_LARGE_WARNING = 6L;
        public static final Long HEART_ABNORMAL_WARNING = 7L;
    }
}
