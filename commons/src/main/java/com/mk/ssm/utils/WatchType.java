package com.mk.ssm.utils;

/**
 * Created by wang18365266176 on 17-4-19.
 */
public class WatchType {
    public static final String type0="卡片";
    public static final String type1="手表";
    public static final String type2="手环";
    public static final String relate0="未绑定";
    public static final String relate1="绑定中";
    public static final String useless1="报废";
    public static final String useless0="使用中";
    public static final String able0="无权限";
    public static final String able1="有权限";
    public static final String online1="在线";
    public static final String online0="离线";
    public static final String state0="未知";
    public static final String state1="坐";
    public static final String state2="跑";
    public static final String state3="睡";
    public static final Integer state=0;//设备状态未知

    public static String getType(Integer num){
        /**
         * 6
         * 如果设备类型不为空
         * 0:卡片
         * 1：手表
         * 2.手环
         */
        String str = "";
        if (num == 0) {
            str = type0;
        }
        if (num == 1) {
            str = type1;
        }
        if (num == 2) {
            str = type2;
        }
        return str;
    }

    /**
     * 绑定状态参数
     * 已绑定,已解绑
     */
    public static interface boundStatus{
        public static final String bound="已绑定";
        public static final String unbound="已解绑";
        //绑定状态
        public static final Integer unrelate=0;//未绑定
        public static final Integer relate=1;//绑定
    }
    /**
     * 人员类型
     */
    public static interface clientType{
        public static final String doctor="医务人员";
        public static final String patient="病人";
        public static final String visitor="访客";
        public static final String terminal="终端用户";
    }
}
